# Installing Nocodb with docker-compose.

## Quick Installation

**Before starting the instance, direct the domain (subdomain) to the ip of the server where Nocodb will be installed!**

## 1. Nocodb Server Requirements
From and more
- 2 CPUs
- 2 RAM 
- 10 Gb 

Run for Ubuntu 22.04

``` bash
sudo apt-get purge needrestart
```

Install docker and docker-compose:

``` bash
curl -s https://gitlab.com/6ministers/business-apps/nocodb-ssl-docker-compose/-/raw/master/setup.sh | sudo bash -s
```

Download Nocodb instance:


``` bash
curl -s https://gitlab.com/6ministers/business-apps/nocodb-ssl-docker-compose/-/raw/master/download.sh | sudo bash -s nocodb
```

If `curl` is not found, install it:

``` bash
$ sudo apt-get install curl
# or
$ sudo yum install curl
```

Go to the catalog

``` bash
cd nocodb
```

To change the domain in the `Caddyfile` to your own

``` bash
https://nocodb.your-domain.com:443 {
    reverse_proxy 127.0.0.1:8080
	# tls admin@example.org
	encode zstd gzip
	file_server

...	
}
```

**Run Portainer:**

``` bash
docker-compose up -d
```

Then open `https://nocodb.your-domain.com:` to access Nocodb


## Nocodb container management

**Run Nocodb**:

``` bash
docker-compose up -d
```

**Restart**:

``` bash
docker-compose restart
```

**Restart**:

``` bash
sudo docker-compose down && sudo docker-compose up -d
```

**Stop**:

``` bash
docker-compose down
```

## Documentation

https://github.com/nocodb/nocodb

https://www.nocodb.com/
